const robot = require("robotjs")
const moment = require("moment")

let isClick = false
const app = () => {
    const { argv } = process

    const timeStart = argv[2] // eg. "0034"
    const timeFrequence = argv[3] // eg. "200"
    const timeclick = moment(timeStart, "HHmm").format('HHmmssSSS')
    const tick = +timeclick - (+timeFrequence) // standart 150 - 200
    setInterval(() => {
        const time = moment().format('HHmmssSSS')
        
        if (!isClick && time > tick) {
            console.log('timeclick: ', timeclick)
            console.log('tick:      ', tick)
            console.log('time:      ', time)
            console.log('*******')
            isClick = true
            robot.keyTap('enter')
            process.exit(1)
        }
    }, 1)
}

app()

